package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import dao.daoNews;
import dao.daoProduits;
import entities.news;
import entities.produits;


@WebService(endpointInterface = "ws.foodtruck")
public class foodtruckImpl implements foodtruck {
	
	private daoProduits daoprod = new daoProduits();
	private daoNews daonews = new daoNews();
	
	public List<news> LastNews()  {
		return this.daonews.LastNews();
	}
	
	public List<produits> topProduits()  {
		return this.daoprod.topProduits();
	}
	

	public List<produits> findAllProduits(){
		return this.daoprod.findAllProduits();
	}
	
	public List<produits> CategorieSousProduits(int idSousCateg){
		return this.daoprod.CategorieSousProduits(idSousCateg);
	}
	
	public List<produits> dispoProduit(String lettreJour){
		return this.daoprod.dispoProduit(lettreJour);
	}
	
}
