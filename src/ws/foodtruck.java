package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import entities.news;
import entities.produits;


@WebService
public interface foodtruck {
		
		@WebMethod
		public List<news> LastNews();

		@WebMethod
		public List<produits> topProduits();
		
		@WebMethod
		public List<produits> findAllProduits();
		
		@WebMethod
		public List<produits> CategorieSousProduits(int idSousCateg);
		
		@WebMethod
		public List<produits> dispoProduit(String lettreJour);
}
