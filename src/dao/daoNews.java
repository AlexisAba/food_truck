package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.news;


public class daoNews {
	

	static private String DBHOST = "jdbc:mysql://127.0.0.1/food_truck";
	static private String DBUSER = "root";
	static private String DBPSWD = "";
	
	public void create(news n) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("INSERT INTO `news` (`titre`, `description`, `image`) VALUES (?, ?, ?);");
			ps.setString(1, n.getTitre());
			ps.setString(2, n.getDescription());
			ps.setString(3, n.getImage());
		
			ps.executeUpdate();

			ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}
	}
	
	public news findById(Integer id) {

		news n = new news();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `news` WHERE `id`=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				n = new news(rs.getInt("id"), rs.getString("titre"), rs.getString("description"), rs.getString("image"));
			}
				
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			// TODO Auto-generated catch block
			er.printStackTrace();
		}

		
		return n;

	}
	
	public List<news> findAllNews() {

		List<news> news = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM news");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				news.add(new news(rs.getInt("id"), rs.getString("titre"), rs.getString("description"), rs.getString("image")));
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
		return news;
	}
	
	public void update(news e) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("UPDATE `news` SET `titre`=?, `description`=?, `image`=? WHERE `id`=?");
			ps.setString(1, e.getTitre());
			ps.setString(2, e.getDescription());
			ps.setString(3, e.getImage());
			ps.setInt(4, e.getId());
			
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
	}

	// [OK]
	public void delete(Integer id) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `news` WHERE `news`.`id`=?");
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		
	}
	
	
	public List<news> LastNews() {
		List<news> lastNews = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM news ORDER BY id DESC LIMIT 3");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				lastNews.add(new news(rs.getInt("id"), rs.getString("titre"), rs.getString("description"), rs.getString("image")));
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
		return lastNews;

		
	}
	
	
	
}
