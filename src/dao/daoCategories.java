package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entities.categories;

public class daoCategories {	

	static private String DBHOST = "jdbc:mysql://127.0.0.1/food_truck";
	static private String DBUSER = "root";
	static private String DBPSWD = "";
	
	public void create(categories n) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("INSERT INTO `categories` (`id`, `nom`) VALUES (?, ?);");
			ps.setInt(1, n.getId());
			ps.setString(2, n.getNom());
			
		
			ps.executeUpdate();

			ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}
	}
	
	public categories findById(Integer id) {

		categories n = new categories();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `categories` WHERE `id`=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				n = new categories(rs.getInt("id"), rs.getString("nom"));
			}
				
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			// TODO Auto-generated catch block
			er.printStackTrace();
		}

		
		return n;

	}
	
	public List<categories> findAll() {

		List<categories> categories = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM news");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				categories.add(new categories(rs.getInt("id"), rs.getString("nom")));
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
		return categories;
	}
	
	public void update(categories e) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("UPDATE `categories` SET `id`=?, `nom`=?");
			ps.setInt(1, e.getId());
			ps.setString(2, e.getNom());
			
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
	}

	// [OK]
	public void delete(Integer id) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `categories` WHERE `categories`.`id`=?");
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		
	}
	
	
	
	
}