package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import entities.produits;

public class daoProduits {
	static private String DBHOST = "jdbc:mysql://127.0.0.1/food_truck";
	static private String DBUSER = "root";
	static private String DBPSWD = "";
	
	public void create(produits p) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("INSERT INTO `produits` (`nom`, `dispo`, `prix`, note, image, nbrVente, idCategorie) VALUES (?, ?, ?, ?, ?, ?, ?);");
			ps.setString(1, p.getNom());
			ps.setString(2, p.getDispo());
			ps.setInt(3, p.getPrix());
			ps.setInt(4, p.getNote());
			ps.setString(5, p.getImage());
			ps.setInt(6, p.getNbrVente());
			ps.setInt(7, p.getIdCategorie());
		
			ps.executeUpdate();

			ps.close(); conn.close();

		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}
	}
	
	public produits findById(Integer id) {

		produits n = new produits();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM `produits` WHERE `id`=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				n = new produits(rs.getInt("id"), rs.getString("nom"), rs.getString("dispo"), rs.getInt("prix"), rs.getInt("note"), rs.getString("image"), rs.getInt("nbrVente"), rs.getInt("idCategorie"));
			}
				
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			// TODO Auto-generated catch block
			er.printStackTrace();
		}

		
		return n;

	}
	
	public List<produits> findAllProduits() {

		List<produits> produits = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM produits");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				produits.add(new produits(rs.getInt("id"), rs.getString("nom"), rs.getString("dispo"), rs.getInt("prix"), rs.getInt("note"), rs.getString("image"), rs.getInt("nbrVente"), rs.getInt("idCategorie")));
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
		return produits;
	}
	
	public void update(produits e) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("UPDATE `produits` SET `nom`=?, `dispo`=?, `prix`=?, `note`=?, `image`=?, `nbrVente`=? idCategorie=? WHERE `id`=?");
			ps.setString(1, e.getNom());
			ps.setString(2, e.getDispo());
			ps.setInt(3, e.getPrix());
			ps.setInt(4, e.getNote());
			ps.setString(5, e.getImage());
			ps.setInt(6, e.getNbrVente());
			ps.setInt(7, e.getIdCategorie());
			ps.setInt(8, e.getId());
			
			
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
	}

	// [OK]
	public void delete(Integer id) {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `produits` WHERE `produits`.`id`=?");
			ps.setInt(1, id);
			ps.executeUpdate();
			ps.close(); conn.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		
	}
	
	
	public List<produits> topProduits() {
		List<produits> topProduits = new ArrayList();
		
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM produits ORDER BY nbrVente DESC LIMIT 3");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				topProduits.add(new produits(rs.getInt("id"), rs.getString("nom"), rs.getString("dispo"), rs.getInt("prix"), rs.getInt("note"), rs.getString("image"), rs.getInt("nbrVente"), rs.getInt("idCategorie")));
			rs.close(); ps.close(); conn.close();
		
		} catch (ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}

		
		return topProduits;

		
	}
	
	public List<produits> CategorieSousProduits(int idSousCateg){
		List<produits> listProd = new ArrayList();
	
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM produits,souscategorie WHERE produits.idCategorie = souscategorie.id AND idCategorie = ?");
			ps.setInt(1, idSousCateg);
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				listProd.add(new produits(rs.getInt("id"), rs.getString("nom"), rs.getString("dispo"), rs.getInt("prix"), rs.getInt("note"), rs.getString("image"), rs.getInt("nbrVente"), rs.getInt("idCategorie")));
			rs.close(); ps.close(); conn.close();
			
		} catch(ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}
		
		return listProd;
	}
	
	public List<produits> dispoProduit(String lettreJour){
		List<produits> listDispo = new ArrayList();
	
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(DBHOST, DBUSER, DBPSWD);
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM produits WHERE dispo LIKE ?");
			ps.setString(1, "%"+lettreJour+"%");
			ResultSet rs = ps.executeQuery();
			while (rs.next())
				listDispo.add(new produits(rs.getInt("id"), rs.getString("nom"), rs.getString("dispo"), rs.getInt("prix"), rs.getInt("note"), rs.getString("image"), rs.getInt("nbrVente"), rs.getInt("idCategorie")));
			rs.close(); ps.close(); conn.close();
			
		} catch(ClassNotFoundException | SQLException er) {
			er.printStackTrace();
		}
		
		return listDispo;
	}
}
