package entities;

public class categories {
	
	private int id;
	private String nom;
	
		public categories(int id, String nom) {
		this.id = id;
		this.nom = nom;
		
	}
		
		public categories() {}
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Override
	public String toString() {
		return "categories [id=" + id + ", nom=" + nom + "]";
	}
	
	

}
