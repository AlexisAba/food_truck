package entities;

public class produits {
	private int id;
	private String nom;
	private String dispo;
	private int prix;
	private int note;
	private String image;
	private int nbrVente;
	private int idCategorie;
	
	public produits(int id, String nom, String dispo, int prix, int note, String image, int nbrVente, int idCategorie) {
		super();
		this.id = id;
		this.nom = nom;
		this.dispo = dispo;
		this.prix = prix;
		this.note = note;
		this.image = image;
		this.nbrVente = nbrVente;
		this.idCategorie = idCategorie;
	}
	
	public produits(String nom, String dispo, int prix, int note, String image, int nbrVente, int idCategorie) {
		super();
		this.nom = nom;
		this.dispo = dispo;
		this.prix = prix;
		this.note = note;
		this.image = image;
		this.nbrVente = nbrVente;
		this.idCategorie = idCategorie;
	}
	
	public produits() {
		
	}
	
	public int getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(int idCategorie) {
		this.idCategorie = idCategorie;
	}

	public int getNbrVente() {
		return nbrVente;
	}

	public void setNbrVente(int nbrVente) {
		this.nbrVente = nbrVente;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDispo() {
		return dispo;
	}

	public void setDispo(String dispo) {
		this.dispo = dispo;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "produit [id=" + id + ", nom=" + nom + ", dispo=" + dispo + ", prix=" + prix + ", note=" + note
				+ ", image=" + image + "]";
	}
	
	
	
}
