-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 05 fév. 2019 à 13:41
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `food_truck`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Gouter'),
(2, 'Petit dejeuner'),
(3, 'Dejeuner'),
(4, 'Diner');

-- --------------------------------------------------------

--
-- Structure de la table `liaisoncat`
--

DROP TABLE IF EXISTS `liaisoncat`;
CREATE TABLE IF NOT EXISTS `liaisoncat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCateg` int(11) NOT NULL,
  `idSousCateg` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `liaisoncat`
--

INSERT INTO `liaisoncat` (`id`, `idCateg`, `idSousCateg`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 5),
(4, 2, 2),
(5, 2, 4),
(6, 2, 3),
(7, 2, 5),
(8, 3, 1),
(9, 3, 2),
(10, 3, 3),
(11, 3, 4),
(12, 3, 5),
(13, 3, 6),
(14, 4, 1),
(15, 4, 2),
(16, 4, 3),
(17, 4, 4),
(18, 4, 5),
(19, 4, 6);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `news`
--

INSERT INTO `news` (`id`, `titre`, `description`, `image`) VALUES
(1, 'Burger du mois', 'Un nouveau burger est disponible dans le catalogue.', 'Burger'),
(2, 'Titre', 'Lorem ipsum blablabla', 'Lorem ipsum'),
(3, 'jfsdfjk', 'kflsdjfklsdjnfklnsdvkln', 'kjjkfd'),
(4, 'jfsjkjdfkdfjjq', 'jkdsjsdfkjk', 'djqkdjqsk');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) NOT NULL,
  `dispo` varchar(150) NOT NULL,
  `prix` double NOT NULL,
  `note` int(11) NOT NULL,
  `image` varchar(150) NOT NULL,
  `nbrVente` int(11) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `nom`, `dispo`, `prix`, `note`, `image`, `nbrVente`, `idCategorie`) VALUES
(1, 'Tacos poulet', 'L-Ma-V', 5, 2, 'image tacos', 52, 3),
(2, 'burger americain', 'L-Ma-Me-J-V', 8.5, 4, 'Burger', 47, 3),
(3, 'Salade caesar', 'Ma-Me-J', 6.5, 3, 'Salade ', 22, 3),
(4, 'Burritos', 'J-V', 3, 3, 'Burritos mexicain', 12, 3),
(5, 'Coca-cola', 'L-Ma-Me-J-V', 1, 5, 'Coca', 99, 1),
(6, 'Expresso', 'L-Ma-Me-J-V', 1.5, 4, 'Expresso', 74, 2),
(7, 'Moelleux au chocolat', 'L-Ma-Me-J-V', 4.5, 4, 'gateaux chocolat', 18, 5),
(8, 'Milkshake framboise', 'J-V', 6.5, 2, 'milkshake', 8, 5);

-- --------------------------------------------------------

--
-- Structure de la table `souscategorie`
--

DROP TABLE IF EXISTS `souscategorie`;
CREATE TABLE IF NOT EXISTS `souscategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `souscategorie`
--

INSERT INTO `souscategorie` (`id`, `nom`) VALUES
(1, 'Boisson Froide'),
(2, 'Boisson Chaude'),
(3, 'Plat'),
(4, 'Entrée'),
(5, 'Dessert'),
(6, 'Aperitif');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
